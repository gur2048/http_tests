package helpers;

/**
 * Created by v.gurov on 28.04.2016.
 */
public enum TroopsGroupId {

    INFANTRY(0),
    INFANTRY2(1),
    ARTILLERY(2),
    ARTILLERY2(3),
    ARMOURED(4),
    ARMOURED2(5),
    AEROSPACE(6),
    AEROSPACE2(7),
    ROBOTS(8),
    BUGS(9),
    STRATEGY(10),
    INCUBATOR(11),
    INCUBATOR2(12),
    AVP(13),
    MISSILE(20);
    //TACTICAL_BUILDING:int = 80;

    int groupId;

    TroopsGroupId(int groupId)
    {
        this.groupId = groupId;
    }

    public int getGroupId() {
        return groupId;
    }


}
