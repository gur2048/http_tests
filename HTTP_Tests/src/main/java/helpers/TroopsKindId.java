package helpers;

public enum TroopsKindId {

    ATTACKING(0),
    DEFENSIVE(1),
    RECON(2);

    int kindId;

    TroopsKindId(int kindId)
    {
        this.kindId = kindId;
    }

    public int getKindId() {
        return kindId;
    }

}
