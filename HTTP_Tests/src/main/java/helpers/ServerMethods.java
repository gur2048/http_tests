package helpers;

public enum ServerMethods {

	GET_TOUCH("GetTouch"),
	SIGN_IN("SignIn"),
	BLACK_MARKET_BUY_ITEM("BlackMarket.BuyItem"),
	BLACK_MARKET_BUY("BlackMarket.Buy");


	private String methodName;

	ServerMethods(String xpath) {
		this.methodName = xpath;
	}

	public String getMethodName() {
		return methodName;
	}
}
