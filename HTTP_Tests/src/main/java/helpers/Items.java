package helpers;

/**
 * Created by guska on 19.04.16.
 */
public enum Items {

        MOVEMENT_BOST_075(700, 400),
        MOVEMENT_BOST_050(701, 700);

        private int id;
        private int price;

    Items(int id, int price)
        {
            this.id = id;
            this.price = price;
        }

    public int getId() {
        return id;
    }

    public int getPrice() {
        return price;
    }
}
