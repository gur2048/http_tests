package helpers;

/**
 * Created by v.gurov on 28.04.2016.
 */
public enum DiscountRouletteOfferKind {

    DRAWINGS(1),
    RESOURCE_KITS(2),
    CITY_SKINS(3),
    RESOURCE_CONVERSION(4),
    SECTOR_EXTENSION (5),
    TOWER_UPGRADE (6),
    CONSTRUCTION_WORKER(7),
    BOOST(8),
    BUILDINGS(9),
    TROOPS(10),
    RESURRECTION(11),
    PAYMENTS(12),
    BLACKMARKET_ITEMS(13),
    INSTANTUNIT_RETURN(14),
    ALLIANCE_CREATING(15);

    int kindId;

    DiscountRouletteOfferKind(int kindId)
    {
        this.kindId = kindId;
    }

    public int getKindId() {
        return kindId;
    }
}
