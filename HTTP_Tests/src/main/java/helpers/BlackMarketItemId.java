package helpers;

/**
 * Created by guska on 15.05.16.
 */
public enum BlackMarketItemId {

    esourceBoostU25Free(200),
    ResourceBoostT25Free(201),
    ResourceBoostM25Free(202),
    ResourceBoostU25(203),
    ResourceBoostT25(204),
    ResourceBoostM25(205),
    ResourceBoost25D3(206), //TDR Server

    Boost1Minute(300),
    Boost3Minutes(301),
    Boost5Minutes(302),
    Boost10Minutes(303),
    Boost15Minutes(304),
    Boost30Minutes(305),
    Boost1Hour(306),
    Boost4Hours(307),
    Boost8Hours(308),
    Boost12Hours(309),
    Boost1Day(310),
    Boost5MinutesBonus(311),
    Boost10MinutesBonus(312),
    Boost15MinutesBonus(313),
    Boost30MinutesBonus(314),
    Boost1HourBonus(315),
    Boost4HoursBonus(316),
    Boost8HoursBonus(317),
    Boost1DayBonus(318),
    Boost12HoursBonus(319),

    Gem2ChestsLvl1(400),
    Gem2ChestsLvl2(401),
    Gem2ChestsLvl3(402),
    Gem2ChestsLvl2To4(403),
    Gem2ChestsLvl3To5(404),

    Gem1ChestLvl1Bonus(405),
    Gem2ChestsLvl1Bonus(406),
    Gem2ChestsLvl1To3Bonus(407),
    Gem2ChestsLvl2Bonus(408),
    Gem2ChestsLvl3Bonus(409),
    Gem2ChestsLvl2To4Bonus(410),
    Gem2ChestsLvl3To5Bonus(411),

    GemExtractorLvl1To4(450),
    GemExtractorLvl1To8(451),
    GemExtractorLvl1To12(452),
    GemExtractorLvl1To4Bonus(453),
    GemExtractorLvl1To8Bonus(454),
    GemExtractorLvl1To12Bonus(455),

    VipActivator30MinutesBonus(500),
    VipActivator60MinutesBonus(501),
    VipActivator1DayBonus(502),
    VipActivator2DaysLvl0To3(503),
    VipActivator7DaysLvl0To3(504),
    VipActivator30DaysLvl0To3(505),
    VipActivator2DaysLvl4To6(506),
    VipActivator7DaysLvl4To6(507),
    VipActivator30DaysLvl4To6(508),
    VipActivator2DaysLvl7(509),
    VipActivator7DaysLvl7(510),
    VipActivator30DaysLvl7(511),
    VipActivator2DaysLvl8To9(512),
    VipActivator7DaysLvl8To9(513),
    VipActivator30DaysLvl8To9(514),
    VipActivator2DaysLvl10(515),
    VipActivator7DaysLvl10(516),
    VipActivator30DaysLvl10(517),
    VipActivator2DaysReturned(518),
    VipActivator2DaysLvl0To3Bonus(519),
    VipActivator7DaysLvl7Bonus(520),
    VipActivator7DaysLvl0To3Bonus(521),
    VipActivator2DaysBonus(522),
    VipActivator7DaysBonus(523),
    VipActivator30DaysBonus(524),


    VipPoints5(601),
    VipPoints75(602),
    VipPoints150(603),
    VipPoints600(604),
    VipPoints40(605),
    VipPoints100(606),
    VipPoints20(607),
    VipPoints1(608),

    MovementBoost075(700),
    MovementBoost05(701),
    MovementBoost075Bonus(702),
    MovementBoost05Bonus(703),

    SlotActivator(800),

    ItemRenameSector(900),
    ItemSetAvatar(901),
    ItemSelectCharacter(902),

    ResourceGoldMoney5(1000),
    ResourceGoldMoney10(1001),
    ResourceGoldMoney15(1002),
    ResourceGoldMoney20(1003),
    ResourceGoldMoney50(1004),
    ResourceG700(1005), //TDR Server
    ResourceG1250(1006), //TDR Server
    ResourceG1500(1007), //TDR Server
    ResourceG2000(1008), //TDR Server
    ResourceG6350(1009), //TDR Server
    ResourceG12000(10010), //TDR Server

    SkillPointsDiscard(1500),
    ConstructionPoints(1600),

    SectorTeleport(1700),
    SectorTeleportRandom(1701),

    ResourceU300(2010),
    ResourceU400(2011),
    ResourceU1000(2020),
    ResourceU2000(2021),
    ResourceU3000(2022),
    ResourceU5000(2023),
    ResourceU8000(2024),
    ResourceU10000(2025),
    ResourceU20000(2026),
    ResourceU30000(2027),
    ResourceU50000(2028),
    ResourceU100000(2029),
    ResourceU4000(2030), //TDR Server
    ResourceU500(2031), //TDR Server
    ResourceU100(2032), //TDR Server

    ResourceT300(2040),
    ResourceT400(2041),
    ResourceT1000(2050),
    ResourceT2000(2051),
    ResourceT3000(2052),
    ResourceT5000(2053),
    ResourceT8000(2054),
    ResourceT10000(2055),
    ResourceT20000(2056),
    ResourceT30000(2057),
    ResourceT50000(2058),
    ResourceT100000(2059),
    ResourceT4000(2060), //TDR Server
    ResourceT500(2061), //TDR Server
    ResourceT100(2062), //TDR Server

    ResourceM300(2080),
    ResourceM400(2081),
    ResourceM1000(2090),
    ResourceM2000(2091),
    ResourceM3000(2092),
    ResourceM5000(2093),
    ResourceM8000(2094),
    ResourceM10000(2095),
    ResourceM20000(2096),
    ResourceM30000(2097),
    ResourceM50000(2098),
    ResourceM100000(2099),
    ResourceM4000(2100), //TDR Server
    ResourceM500(2101), //TDR Server
    ResourceM100(2102), //TDR Server

    InventoryKeyCommonTier1(2500),
    InventoryKeyUncommonTier1(2501),
    InventoryKeyRareTier1(2502),
    InventoryKeyEpicTier1(2503),
    InventoryKeyCommonTier2(2504),
    InventoryKeyUncommonTier2(2505),
    InventoryKeyRareTier2(2506),
    InventoryKeyEpicTier2(2507),
    InventoryKeyCommonTier3(2508),
    InventoryKeyUncommonTier3(2509),
    InventoryKeyRareTier3(2510),
    InventoryKeyEpicTier3(2511),
    InventoryKeyCommonTier4(2512),
    InventoryKeyUncommonTier4(2513),
    InventoryKeyRareTier4(2514),
    InventoryKeyEpicTier4(2515),

    StaticBonusPackAllianceMissionStep1(2600),
    StaticBonusPackAllianceMissionStep2(2601),
    StaticBonusPackAllianceMissionStep3(2602),
    StaticBonusPackAllianceMissionStep4(2603),

    TroopsAirUnit1Pack5(3001), //TDR Server
    TroopsAirUnit1Pack10(3002), //TDR Server
    TroopsAirUnit1Pack20(3003), //TDR Server
    TroopsAirUnit1Pack30(3004), //TDR Server
    TroopsAirUnit1Pack40(3005), //TDR Server
    TroopsArmoredUnit3Pack5(3006), //TDR Server
    TroopsArmoredUnit3Pack10(3007), //TDR Server
    TroopsArmoredUnit3Pack20(3008), //TDR Server
    TroopsArmoredUnit3Pack30(3009), //TDR Server
    TroopsArmoredUnit3Pack40(3010), //TDR Server
    TroopsAirUnit2Pack3(3011), //TDR Server
    TroopsAirUnit2Pack5(3012), //TDR Server
    TroopsAirUnit2Pack10(3013), //TDR Server
    TroopsAirUnit2Pack15(3014), //TDR Server
    TroopsAirUnit2Pack20(3015), //TDR Server
    TroopsAirUnit2Pack30(3016), //TDR Server
    TroopsSpecialForcesInfantryUnit4Pack5(3017), //TDR Server
    TroopsSpecialForcesInfantryUnit4Pack15(3018), //TDR Server
    TroopsSpecialForcesInfantryUnit4Pack20(3019), //TDR Server
    TroopsSpecialForcesInfantryUnit4Pack30(3020), //TDR Server
    TroopsArtilleryUnit3Pack3(3021), //TDR Server
    TroopsArtilleryUnit3Pack10(3022), //TDR Server
    TroopsArtilleryUnit3Pack20(3023), //TDR Server
    TroopsArtilleryUnit3Pack30(3024), //TDR Server
    TroopsArtilleryUnit4Pack10(3025), //TDR Server
    TroopsArtilleryUnit4Pack15(3026), //TDR Server
    TroopsArtilleryUnit4Pack20(3027), //TDR Server
    TroopsArtilleryUnit4Pack30(3028), //TDR Server
    TroopsAirUnit4Pack5(3029), //TDR Server
    TroopsAirUnit4Pack10(3030), //TDR Server
    TroopsAirUnit4Pack15(3031), //TDR Server
    TroopsAirUnit4Pack30(3032), //TDR Server
    TroopsInfantryUnit4Pack10(3033), //TDR Server
    TroopsInfantryUnit4Pack3(3034), //TDR Server
    TroopsInfantryUnit4Pack5(3035), //TDR Server
    TroopsInfantryUnit4Pack7(3036), //TDR Server
    TroopsInfantryUnit1Pack5(3037), //TDR Server
    TroopsInfantryUnit4Pack1(3038), //TDR Server
    TroopsInfantryUnit4Pack2(3039), //TDR Server
    TroopsAirUnit1Pack1(3040), //TDR Server
    TroopsInfantryUnit1Pack2(3041), //TDR Server
    TroopsInfantryUnit2Pack2(3042), //TDR Server
    TroopsAirUnit1Pack2(3043), //TDR Server
    TroopsInfantryUnit3Pack2(3044), //TDR Server
    TroopsArmoredUnit1Pack2(3045), //TDR Server
    TroopsArmoredUnit2Pack2(3046), //TDR Server
    TroopsArmoredUnit3Pack2(3047), //TDR Server
    TroopsDefensiveInfantryUnit1Pack2(3048), //TDR Server
    TroopsDefensiveInfantryUnit2Pack2(3049), //TDR Server
    TroopsArtilleryUnit1Pack2(3050), //TDR Server
    TroopsDefensiveInfantryUnit3Pack2(3051), //TDR Server
    TroopsSpecialForcesInfantryUnit1Pack2(3052), //TDR Server
    TroopsArtilleryUnit2Pack2(3053), //TDR Server
    TroopsDefensiveInfantryUnit4Pack2(3054), //TDR Server
    TroopsSpecialForcesInfantryUnit2Pack2(3055), //TDR Server
    TroopsArtilleryUnit3Pack2(3056), //TDR Server
    TroopsSpecialForcesInfantryUnit3Pack2(3057), //TDR Server
    TroopsArtilleryUnit4Pack2(3058), //TDR Server
    TroopsAirUnit2Pack2(3059), //TDR Server
    TroopsAirUnit3Pack2(3060), //TDR Server
    TroopsAirUnit4Pack2(3061), //TDR Server
    TroopsSpecialForcesInfantryUnit4Pack2(3062), //TDR Server
    TroopsInfantryUnit1Pack3(3063), //TDR Server
    TroopsInfantryUnit2Pack3(3064), //TDR Server
    TroopsAirUnit1Pack3(3065), //TDR Server
    TroopsInfantryUnit3Pack3(3066), //TDR Server
    TroopsArmoredUnit1Pack3(3067), //TDR Server
    TroopsArmoredUnit2Pack3(3068), //TDR Server
    TroopsArmoredUnit3Pack3(3069), //TDR Server
    TroopsDefensiveInfantryUnit1Pack3(3070), //TDR Server
    TroopsDefensiveInfantryUnit2Pack3(3071), //TDR Server
    TroopsArtilleryUnit1Pack3(3072), //TDR Server
    TroopsDefensiveInfantryUnit3Pack3(3073), //TDR Server
    TroopsSpecialForcesInfantryUnit1Pack3(3074), //TDR Server
    TroopsArtilleryUnit2Pack3(3075), //TDR Server
    TroopsDefensiveInfantryUnit4Pack3(3076), //TDR Server
    TroopsSpecialForcesInfantryUnit2Pack3(3077), //TDR Server
    TroopsSpecialForcesInfantryUnit3Pack3(3078), //TDR Server
    TroopsArtilleryUnit4Pack3(3079), //TDR Server
    TroopsAirUnit3Pack3(3080), //TDR Server
    TroopsAirUnit4Pack3(3081), //TDR Server
    TroopsSpecialForcesInfantryUnit4Pack3(3082), //TDR Server
    TroopsInfantryUnit1Pack4(3083), //TDR Server
    TroopsInfantryUnit2Pack4(3084), //TDR Server
    TroopsAirUnit1Pack4(3085), //TDR Server
    TroopsInfantryUnit3Pack4(3086), //TDR Server
    TroopsInfantryUnit4Pack4(3087), //TDR Server
    TroopsArmoredUnit1Pack4(3088), //TDR Server
    TroopsArmoredUnit2Pack4(3089), //TDR Server
    TroopsArmoredUnit3Pack4(3090), //TDR Server
    TroopsDefensiveInfantryUnit1Pack4(3091), //TDR Server
    TroopsDefensiveInfantryUnit2Pack4(3092), //TDR Server
    TroopsArtilleryUnit1Pack4(3093), //TDR Server
    TroopsDefensiveInfantryUnit3Pack4(3094), //TDR Server
    TroopsSpecialForcesInfantryUnit1Pack4(3095), //TDR Server
    TroopsArtilleryUnit2Pack4(3096), //TDR Server
    TroopsDefensiveInfantryUnit4Pack4(3097), //TDR Server
    TroopsSpecialForcesInfantryUnit2Pack4(3098), //TDR Server
    TroopsArtilleryUnit3Pack4(3099), //TDR Server
    TroopsSpecialForcesInfantryUnit3Pack4(3100), //TDR Server
    TroopsArtilleryUnit4Pack4(3101), //TDR Server
    TroopsAirUnit2Pack4(3102), //TDR Server
    TroopsAirUnit3Pack4(3103), //TDR Server
    TroopsAirUnit4Pack4(3104), //TDR Server
    TroopsSpecialForcesInfantryUnit4Pack4(3105), //TDR Server
    TroopsInfantryUnit2Pack5(3106), //TDR Server
    TroopsInfantryUnit3Pack5(3107), //TDR Server
    TroopsArmoredUnit1Pack5(3108), //TDR Server
    TroopsArmoredUnit2Pack5(3109), //TDR Server
    TroopsDefensiveInfantryUnit1Pack5(3110), //TDR Server
    TroopsDefensiveInfantryUnit2Pack5(3111), //TDR Server
    TroopsArtilleryUnit1Pack5(3112), //TDR Server
    TroopsDefensiveInfantryUnit3Pack5(3113), //TDR Server
    TroopsSpecialForcesInfantryUnit1Pack5(3114), //TDR Server
    TroopsArtilleryUnit2Pack5(3115), //TDR Server
    TroopsDefensiveInfantryUnit4Pack5(3116), //TDR Server
    TroopsSpecialForcesInfantryUnit2Pack5(3117), //TDR Server
    TroopsArtilleryUnit3Pack5(3118), //TDR Server
    TroopsSpecialForcesInfantryUnit3Pack5(3119), //TDR Server
    TroopsArtilleryUnit4Pack5(3120), //TDR Server
    TroopsAirUnit3Pack5(3121), //TDR Server
    TroopsInfantryUnit1Pack6(3122), //TDR Server
    TroopsInfantryUnit2Pack6(3123), //TDR Server
    TroopsAirUnit1Pack6(3124), //TDR Server
    TroopsInfantryUnit3Pack6(3125), //TDR Server
    TroopsInfantryUnit4Pack6(3126), //TDR Server
    TroopsArmoredUnit1Pack6(3127), //TDR Server
    TroopsArmoredUnit2Pack6(3128), //TDR Server
    TroopsArmoredUnit3Pack6(3129), //TDR Server
    TroopsDefensiveInfantryUnit1Pack6(3130), //TDR Server
    TroopsDefensiveInfantryUnit2Pack6(3131), //TDR Server
    TroopsArtilleryUnit1Pack6(3132), //TDR Server
    TroopsDefensiveInfantryUnit3Pack6(3133), //TDR Server
    TroopsSpecialForcesInfantryUnit1Pack6(3134), //TDR Server
    TroopsArtilleryUnit2Pack6(3135), //TDR Server
    TroopsDefensiveInfantryUnit4Pack6(3136), //TDR Server
    TroopsSpecialForcesInfantryUnit2Pack6(3137), //TDR Server
    TroopsArtilleryUnit3Pack6(3138), //TDR Server
    TroopsSpecialForcesInfantryUnit3Pack6(3139), //TDR Server
    TroopsArtilleryUnit4Pack6(3140), //TDR Server
    TroopsAirUnit2Pack6(3141), //TDR Server
    TroopsAirUnit3Pack6(3142), //TDR Server
    TroopsAirUnit4Pack6(3143), //TDR Server
    TroopsSpecialForcesInfantryUnit4Pack6(3144), //TDR Server
    TroopsInfantryUnit4Pack12(3145), //TDR Server
    TroopsAirUnit2Pack7(3146), //TDR Server
    TroopsAirUnit3Pack7(3147), //TDR Server
    TroopsAirUnit3Pack10(3148), //TDR Server
    TroopsAirUnit4Pack7(3149), //TDR Server
    TroopsArmoredUnit1Pack10(3150), //TDR Server
    TroopsDefensiveInfantryUnit1Pack50(3151), //TDR Server
    TroopsArmoredUnit2Pack10(3152), //TDR Server
    TroopsAirUnit4Pack20(3153), //TDR Server
    TroopsAirUnit2Pack50(3154), //TDR Server
    TroopsAirUnit2Pack1(3155), //TDR Server
    TroopsArmoredUnit2Pack1(3156), //TDR Server
    TroopsAirUnit4Pack50(3157), //TDR Server
    TroopsAirUnit1Pack50(3158), //TDR Server
    TroopsAirUnit1Pack100(3159), //TDR Server
    TroopsAirUnit1Pack150(3160), //TDR Server
    TroopsInfantryUnit3Pack25(3161), //TDR Server
    TroopsInfantryUnit3Pack10(3162), //TDR Server
    TroopsAirUnit2Pack25(3163), //TDR Server
    TroopsAirUnit4Pack100(3164), //TDR Server
    TroopsAirUnit3Pack15(3165), //TDR Server
    TroopsInfantryUnit4Pack15(3166), //TDR Server
    TroopsArmoredUnit4Pack2(3167), //TDR Server
    TroopsArtilleryUnit4Pack1(3168), //TDR Server
    TroopsAirUnit4Pack1(3169), //TDR Server
    TroopsAirUnit4Pack25(3170), //TDR Server

    ConstructionWorker(5001), //TDR Server
    TestSubItems(5002), //TDR Server
    SectorSkin5(5003), //TDR Server

    DrawingTransportService(5500), //TDR Server
    DrawingChamberOfCommerce(5501), //TDR Server
    DrawingMotorizedInfantryFactory(5502), //TDR Server
    DrawingFinancialCorporation(5503), //TDR Server

    ItemArtifactTypeId1(6001), //TDR Server
    ItemArtifactTypeId2(6002), //TDR Server
    ItemArtifactTypeId3(6003), //TDR Server
    ItemArtifactTypeId4(6004), //TDR Server
    ItemArtifactTypeId5(6005), //TDR Server
    ItemArtifactTypeId6(6006), //TDR Server

    ItemUpgradeBuildingPack1(2700),
    ItemUpgradeBuildingPack10(2701),
    ItemUpgradeBuildingPack100(2702),
    ItemUpgradeBuildingPack500(2703),
    ItemUpgradeTechnology1Pack1(2800),
    ItemUpgradeTechnology1Pack10(2801),
    ItemUpgradeTechnology1Pack100(2802),
    ItemUpgradeTechnology1Pack500(2803),
    ItemUpgradeTechnology2Pack1(2900),
    ItemUpgradeTechnology2Pack10(2901),
    ItemUpgradeTechnology2Pack100(2902),
    ItemUpgradeTechnology2Pack500(2903);



    int itemId;

    BlackMarketItemId(int itemId)
    {
        this.itemId = itemId;
    }

    public int getItemId() {
        return itemId;
    }
}
