package helpers;

public enum StaticDiscountTypeId {

    BLACK_MARKET(1),
    RESURRECTION(2),
    BOOSTERS(3),
    GEMS(4);

    StaticDiscountTypeId(int id)
    {
        this.id = id;
    }

    private int id;

    public int getId() {
        return id;
    }



}
