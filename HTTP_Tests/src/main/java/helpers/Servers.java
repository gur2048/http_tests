package helpers;

/**
 * Created by guska on 16.04.16.
 */
public enum Servers {

    GEO_SUPER_TEST("http://173.244.167.10/Geo/Segment00/segment.ashx",
            190956),
    PIRATES_SUPER_TEST("http://173.244.167.10/Pirates/Segment00/segment.ashx",
            186591),
    ELVES_SUPER_TEST("http://173.244.167.10/Elves/Segment00/segment.ashx",
            182621),
    GEO_VK("http://206.222.5.114/GeoVk/Segment00/segment.ashx",
            "http://206.222.5.114/GeoVk/Segment01/segment.ashx",
            "http://206.222.5.114/GeoVk/Segment02/segment.ashx",
            8336);


    private String segment00;
    private String segment01;
    private String segment02;
    private String serverUrl;

    private int consoleServerId;

    Servers(String segment00, int consoleServerId)
    {
        this.segment00 = segment00;
        this.consoleServerId = consoleServerId;
    }

    Servers(String segment00, String segment01, int consoleServerId)
    {
        this.segment00 = segment00;
        this.segment01 = segment01;
        this.consoleServerId = consoleServerId;
    }

    Servers(String segment00, String segment01, String segment02, int consoleServerId)
    {
        this.segment00 = segment00;
        this.segment01 = segment01;
        this.segment02 = segment02;
        this.consoleServerId = consoleServerId;
    }

    public String getSuperTestServerUrl(String segmentNum)
    {
        serverUrl = segment00.replace("Segment00","Segment" + segmentNum);
        return serverUrl;
    }

    public String getSegment00() {
        return segment00;
    }

    public String getSegment01() {
        return segment01;
    }

    public String getSegment02() {
        return segment02;
    }

    public int getConsoleServerId() {
        return consoleServerId;
    }

}
