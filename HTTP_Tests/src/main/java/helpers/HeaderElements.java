package helpers;

public enum HeaderElements {

    WEATHER(".//*[@id='orb-nav-links']//*[.='Weather']");

    private String xpath;

    HeaderElements(String xpath) {
        this.xpath = xpath;
    }

    public String getXpath() {
        return xpath;
    }
}