package helpers;

public enum TroopsTypeId {

    INFANTRY_UNIT_1(1, TroopsKindId.ATTACKING, TroopsGroupId.INFANTRY),
    INFANTRY_UNIT_2(2, TroopsKindId.DEFENSIVE, TroopsGroupId.INFANTRY),
    INFANTRY_UNIT_3(3, TroopsKindId.DEFENSIVE, TroopsGroupId.INFANTRY),
    INFANTRY_UNIT_4(4, TroopsKindId.ATTACKING, TroopsGroupId.INFANTRY);



    int id;
    String name;
    TroopsGroupId troopsGroupId;
    TroopsKindId troopsKindId;

    TroopsTypeId(int id, TroopsKindId troopsKindId, TroopsGroupId troopsGroupId)
    {
        this.id = id;
        this.troopsGroupId = troopsGroupId;
        this.troopsKindId = troopsKindId;
    }

    public String getName() {
        return name;
    }

    public TroopsGroupId getTroopsGroupId() {
        return troopsGroupId;
    }

    public TroopsKindId getTroopsKindId() {
        return troopsKindId;
    }
}
