package gbo;

import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GboConfigReader {

    public Map<String, String> getGboUserData() throws IOException {
        File file = new File("src/test/resources/config/gbo_userdata.xml");
        Document document = null;
        Map<String, String> getGboUserData = new HashMap<String, String>();
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(file);
            getGboUserData.put("password", document.getElementsByTagName("password").item(0).getTextContent());
            getGboUserData.put("login", document.getElementsByTagName("login").item(0).getTextContent());
            getGboUserData.put("gbourl", document.getElementsByTagName("gbourl").item(0).getTextContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getGboUserData;
    }
}
