package gbo;

import utils.HttpUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GboCommands {

    HttpUtil httpUtil;

    List<String[]> listOfheaders = Arrays.asList(
            new String[]{"Content-Type", "application/json"},
            new String[]{"Host", "gbo.x-plarium.com"},
            new String[]{"Accept", "*/*"},
            new String[]{"Accept-Language", "en-US,en;q=0.5"},
            new String[]{"Accept-Encoding", "gzip, deflate, br"},
            new String[]{"X-Requested-With", "XMLHttpRequest"},
            new String[]{"Referer", "https://gbo.x-plarium.com/games/console"},
            new String[]{"Connection", "keep-alive"}
    );

    public void gboUserSetLevel(int level, String Id, int serverConsoleID, String cookie) throws IOException {

        httpUtil = new HttpUtil();

        String postBody = "{\"name\":\"user.setlevel\",\"params\":[\"level\",\"uid\"],\"values\":[\"" + level
                + "\",\"" + Id + "\"],\"consoleServerId\":\"" + serverConsoleID + "\"}";

        listOfheaders = new ArrayList<>(listOfheaders);
        listOfheaders.add(new String[]{"Cookie", cookie});

        System.out.println(httpUtil.requestPatternForGBOCommand(postBody, listOfheaders));
    }

    public void gboUserResetAllPurschasesCount(String Id, int serverConsoleID, String cookie) throws IOException {

        httpUtil = new HttpUtil();

        String postBody = "{\"name\":\"user.ResetAllPurschasesCount\",\"params\":[\"uid\"],\"values\":[\"" +
                Id + "\"],\"consoleServerId\":\""  + serverConsoleID + "\"}";

        listOfheaders = new ArrayList<>(listOfheaders);
        listOfheaders.add(new String[]{"Cookie", cookie});

        System.out.println(httpUtil.requestPatternForGBOCommand(postBody, listOfheaders));
    }

    public void gboUserGiveGoldMoney (int amount, String Id, int serverConsoleID, String cookie) throws IOException {

        httpUtil = new HttpUtil();

        String postBody = "{\"name\":\"user.givegoldMoney\",\"params\":[\"amount\",\"uid\"],\"values\":[\""
                + amount + "\",\""+ Id +"\"],\"consoleServerId\":\"" + serverConsoleID + "\"}";

        listOfheaders = new ArrayList<>(listOfheaders);
        listOfheaders.add(new String[]{"Cookie", cookie});

        System.out.println(httpUtil.requestPatternForGBOCommand(postBody, listOfheaders));
    }


}
