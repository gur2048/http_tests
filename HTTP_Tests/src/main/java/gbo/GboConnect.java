package gbo;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import utils.HttpUtil;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

public class GboConnect {

    HttpUtil httpUtil;
    GboConfigReader gboConfigReader;

    public String gboLogin() throws URISyntaxException, IOException {

        httpUtil = new HttpUtil();
        gboConfigReader = new GboConfigReader();

        String url = gboConfigReader.getGboUserData().get("gbourl");
        String postBody = "";
        String cookie = "";

        List<NameValuePair> listOfParameters = Arrays.asList(
                new BasicNameValuePair("login", gboConfigReader.getGboUserData().get("login")),
                new BasicNameValuePair("password", gboConfigReader.getGboUserData().get("password")),
                new BasicNameValuePair("remember", "false")
        );

        java.net.URI uri = new URIBuilder(url)
                .addParameters(listOfParameters)
                .build();

        CloseableHttpClient httpClient = HttpClients.createDefault();

        HttpPost httpPostRequest = new HttpPost(uri);

        CloseableHttpResponse response = httpClient.execute(httpPostRequest);

        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            if (header.getName().contains("Cookie"))
            {
                cookie = header.getValue();
            }
        }
        httpUtil.getResponseBodyAsString(response);

        return cookie;
    }
}
