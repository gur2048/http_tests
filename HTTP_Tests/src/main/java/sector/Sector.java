package sector;

import helpers.ServerMethods;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.HttpUtil;
import utils.JsonUtil;
import utils.ResponseUtil;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Sector {

    HttpUtil httpUtil;
    JsonUtil jsonUtil;
    ResponseUtil responseUtil;

    public String getStaticDataAsString(String url) throws IOException, URISyntaxException {

        httpUtil = new HttpUtil();
        responseUtil = new ResponseUtil();

        List<NameValuePair> listOfParameters = Arrays.asList(
                new BasicNameValuePair("method","Client.GetStaticData"),
                new BasicNameValuePair("sign","e72420761056b461861d7bc5cdd1c4e1"),
                new BasicNameValuePair("data","\"B/s0QSujvdtOcQZOJKroeg==\""),
                new BasicNameValuePair("locale", "en-US"),
                new BasicNameValuePair("ver", "900")
        );
        return responseUtil.responseJsonStringCut(httpUtil.getGetRequestResponse(url, listOfParameters));
    }

    public JSONObject getStaticDataAsJSON(String url) throws IOException, URISyntaxException {

        httpUtil = new HttpUtil();
        responseUtil = new ResponseUtil();
        jsonUtil = new JsonUtil();

        List<NameValuePair> listOfParameters = Arrays.asList(
                new BasicNameValuePair("method","Client.GetStaticData"),
                new BasicNameValuePair("sign","e72420761056b461861d7bc5cdd1c4e1"),
                new BasicNameValuePair("data","\"B/s0QSujvdtOcQZOJKroeg==\""),
                new BasicNameValuePair("locale", "en-US"),
                new BasicNameValuePair("ver", "900")
        );
        return jsonUtil.stringToJSON(responseUtil.responseJsonStringCut
                (httpUtil.getGetRequestResponse(url, listOfParameters)));
    }

    public JSONObject getUserDataAsJSON(String url, String userID) throws Exception {

        jsonUtil = new JsonUtil();
        httpUtil = new HttpUtil();
        responseUtil = new ResponseUtil();
        List<String[]> listOfKeys;
        String requestBody;

        List<String[]> listOfheaders = Arrays.asList(
                new String[]{"server-method", ServerMethods.SIGN_IN.getMethodName()},
                new String[]{"Content-type", "text/html"},
                new String[]{"signin-userId", userID},
                new String[]{"signin-authKey", "ba27b979dc7980f41e0d77fe65b59068"},
                new String[]{"Access-key", "B5CC2AC393234bf89DDB5AA216B64668"},
                new String[]{"sign-code", "3a9ef6259272ff2b878bc6e4d93e6c08"});

        listOfKeys = Arrays.asList(
                new String[]{"i", userID, "000"},
                new String[]{"i", "000", "000"}
        );

        requestBody = jsonUtil.changeJSON_byKeys(
                jsonUtil.getStringJSONfromFile("src/test/resources/MethodsBody/SignIn.txt"),
                listOfKeys);

        String jsonString = httpUtil.getPostRequestResponse(
                url,
                requestBody,
                listOfheaders);

        return jsonUtil.stringToJSON(responseUtil.responseJsonStringCut(jsonString));
    }

    public HashMap<Integer, HashMap<String, Double>> getBlackMarketItemsPriceFromStaticData(JSONObject staticData) throws Exception {
        jsonUtil = new JsonUtil();

        HashMap<Integer, HashMap<String, Double>> itemsInfo = new HashMap<Integer, HashMap<String, Double>>();

        JSONArray blackMarketItemsArray = jsonUtil.getJSONKeyValueAsJSONArray(staticData, "bk:i");
        for (int i = 0; i<blackMarketItemsArray.length(); i++)
        {
            JSONObject BlackMarketItemInfo = (JSONObject) blackMarketItemsArray.get(i);
            int itemId = Integer.parseInt(jsonUtil.getJSONKeyValueAsString(BlackMarketItemInfo, "i"));
            double itemPrice = Double.parseDouble(jsonUtil.getJSONKeyValueAsString(BlackMarketItemInfo, "p:g"));
            HashMap<String, Double> itemsPrice = new HashMap<String, Double>();
            itemsPrice.put("g", itemPrice); //Gold Price on Black Market
            itemsInfo.put(itemId, itemsPrice);
        }
        return itemsInfo;
    }

    //get Black Market Units Price From StaticData
    public HashMap<Integer, HashMap<String, Double>> getUnitsPriceFromStaticData(JSONObject staticData) throws Exception {

        jsonUtil = new JsonUtil();

        HashMap<Integer, HashMap<String, Double>> unitsInfo = new HashMap<Integer, HashMap<String, Double>>();

        JSONArray unitPriceArray = jsonUtil.getJSONKeyValueAsJSONArray(staticData, "bk:a");
        for (int i = 0; i<unitPriceArray.length(); i++)
        {
            JSONObject unitBlackMarketInfo = (JSONObject) unitPriceArray.get(i);
            int unitId = Integer.parseInt(jsonUtil.getJSONKeyValueAsString(unitBlackMarketInfo, "t"));
            double unitPrice = Double.parseDouble(jsonUtil.getJSONKeyValueAsString(unitBlackMarketInfo, "p:g"));
            HashMap<String, Double> unitsPrice = new HashMap<String, Double>();
            unitsPrice.put("g", unitPrice); //Gold Price on Black Market
            unitsInfo.put(unitId, unitsPrice);
        }
        return unitsInfo;
    }

    public HashMap<Integer, HashMap<String, Integer>> getAllUnitsFromStaticData(JSONObject staticData) throws Exception {

        jsonUtil = new JsonUtil();

        HashMap<Integer, HashMap<String, Integer>> allUnitsOnGame = new HashMap<Integer, HashMap<String, Integer>>();

        JSONArray unitPriceArray = staticData.getJSONArray("sot");
        for (int j = 0; j < unitPriceArray.length(); j++) {
            JSONObject unitGameInfo = (JSONObject) unitPriceArray.get(j);
            String unitID = jsonUtil.getJSONKeyValueAsString(unitGameInfo, "i");
            String troopsKindId = jsonUtil.getJSONKeyValueAsString(unitGameInfo, "ti:k");
            String troopsGroupId = jsonUtil.getJSONKeyValueAsString(unitGameInfo, "ti:g");
            if (unitID == null || troopsKindId == null || troopsGroupId == null) {

            } else
            {
                HashMap<String, Integer> unitsParam = new HashMap<String, Integer>();
                unitsParam.put("k", Integer.parseInt(troopsKindId)); //TroopsKindId
                unitsParam.put("g", Integer.parseInt(troopsGroupId)); //TroopsGroupId
                allUnitsOnGame.put(Integer.parseInt(unitID), unitsParam);
            }
        }
        return allUnitsOnGame;
    }

    public Integer getRevisionNum (JSONObject userData)
    {
        jsonUtil = new JsonUtil();

        return Integer.valueOf(jsonUtil.getJSONKeyValueAsString(userData, "u:g:r"));
    }

}
