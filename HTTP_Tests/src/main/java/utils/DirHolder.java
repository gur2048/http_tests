package utils;

import java.io.File;

public class DirHolder {

	/**
	 * Gets directory where webdrivers have been downloaded.
	 * @return
	 */
	public static String getDriversDir() {
		return getDir("driverDir", "target/drivers");
	}


	/**
	 * lazy loading mechanism, which is set system property with value and path
	 * @param property - key to find this path again
	 * @param defaultValue - path
	 * @return
	 */
	private static String getDir(String property, String defaultValue) {
		String pValue = System.getProperty(property);
		if (pValue == null) {
			pValue = defaultValue;
		}

		File dir = new File(pValue);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		String result = dir.getAbsolutePath();
		System.setProperty(property, result);
		return result;
	}

}
