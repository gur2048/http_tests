package utils;

import helpers.*;
import sector.Sector;

import java.util.HashMap;
import java.util.List;

/**
 * Created by v.gurov on 28.04.2016.
 */
public class StaticDiscountUtil {

    Sector sector;
    JsonUtil jsonUtil;

    private StaticDiscountTypeId getStaticDiscountType(org.json.JSONObject userData)
    {
        jsonUtil = new JsonUtil();
        String staticDiscountType = jsonUtil.getJSONKeyValueAsString(userData, "asd:0:n:t");
        if (staticDiscountType != null)
        {
            for (StaticDiscountTypeId DiscountType : StaticDiscountTypeId.values()) {
                if (DiscountType.getId() == Integer.parseInt(staticDiscountType))
                {
                    return DiscountType;
                }
            }
        }
        return null;
    }

    private boolean itemIsUnit(int itemId, org.json.JSONObject staticData) throws Exception {
        sector = new Sector();
        HashMap<Integer, HashMap<String, Integer>> allUnitsOnGame = sector.getAllUnitsFromStaticData(staticData);
        if (allUnitsOnGame.containsKey(itemId))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private boolean itemIsGem(int itemId)
    {
        if (itemId > BlackMarketItemId.Gem2ChestsLvl1.getItemId() &&
                itemId < BlackMarketItemId.GemExtractorLvl1To12Bonus.getItemId())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private boolean itemIsBlacMarket(int itemId)
    {
        for (BlackMarketItemId Id : BlackMarketItemId.values()) {
            if (Id.getItemId() == itemId &&
                    itemId > BlackMarketItemId.Boost1Minute.getItemId() - 1 &&
                    itemId < BlackMarketItemId.Boost12HoursBonus.getItemId() + 1) {
                return true;
            }
        }
        return false;
    }

    private boolean itemIsBooster(int itemId)
    {
        if (itemId > BlackMarketItemId.Boost1Minute.getItemId() - 1 &&
                itemId < BlackMarketItemId.Boost12HoursBonus.getItemId() + 1 ||
                itemId > BlackMarketItemId.MovementBoost075.getItemId() - 1  &&
                itemId < BlackMarketItemId.MovementBoost05Bonus.getItemId() + 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Integer getStaticDiscountValue(org.json.JSONObject userData)
    {
        jsonUtil = new JsonUtil();
        return Integer.parseInt(jsonUtil.getJSONKeyValueAsString(userData, "asd:0:n:v"));


    }

    public double expectedPriceCorrectionByStaticDiscount(org.json.JSONObject userData,
                                                          org.json.JSONObject staticData,
                                                          int item) throws Exception {
        StaticDiscountTypeId staticDiscountTypeId = getStaticDiscountType(userData);
        double staticDiscount = getStaticDiscountValue(userData);
        HashMap<Integer, HashMap<String, Double>> itemsPrice = sector.getBlackMarketItemsPriceFromStaticData(staticData);
        HashMap<String, Double> itemInfo = itemsPrice.get(item);
        double itemPirce = itemInfo.get("g");

        if (staticDiscountTypeId == StaticDiscountTypeId.BOOSTERS &&
                itemIsBooster(item))
        {
            return itemPirce * (staticDiscount / 100);
        }
        else if (staticDiscountTypeId == StaticDiscountTypeId.BLACK_MARKET &&
                itemIsBlacMarket(item))
        {
            return itemPirce * (staticDiscount / 100);
        }
        else if (staticDiscountTypeId == StaticDiscountTypeId.GEMS &&
                itemIsGem(item))
        {
            return itemPirce * (staticDiscount / 100);
        }
        else
        {
            return  itemPirce;
        }
    }

    public void check()
    {
        int[] a = {1,2,3,4,5,6,7,8,9,10};
        double[] c = {0.1,0.2,0.3,0.4,0.4,0.5,0.6,0.7,0.8,0.9};
        double[] b = {168.1,168.2,168.3,168.4,168.5,168.6,168.7,168.8,168.9};

        for (int i = 0; i < a.length; i++)
        {
            for (int k = 0; k < b.length; k++)
            {
                System.out.print("|");
                for (int j = 0; j < c.length; j++)
                {
                    System.out.print(Math.round((b[k] * a[i]) * c[j]) + " ");
                }
            }
        }

        System.out.println();

        for (int i = 0; i < a.length; i++)
        {
            for (int k = 0; k < b.length; k++)
            {
                System.out.print("|");
                for (int j = 0; j < c.length; j++)
                {
                    System.out.print(Math.round((b[k] * a[i]) * c[j]) + " ");
                }
            }
        }
    }

}
