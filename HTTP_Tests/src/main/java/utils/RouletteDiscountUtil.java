package utils;

import com.google.common.collect.ImmutableMap;
import helpers.DiscountRouletteOfferKind;
import helpers.Items;
import helpers.TroopsGroupId;
import helpers.TroopsKindId;
import org.json.JSONArray;
import sector.Sector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RouletteDiscountUtil {

    Map<Integer, Items[]>  rouletteDiscountTypes = ImmutableMap.of(
            2129, new Items[]{Items.MOVEMENT_BOST_050, Items.MOVEMENT_BOST_075}
    );

    JsonUtil jsonUtil;

    public int getRouletteDiscountUserSegment(org.json.JSONObject userData)
    {
        jsonUtil = new JsonUtil();
        return Integer.parseInt(jsonUtil.getJSONKeyValueAsString(userData, "dot:0:s"));
    }

    public int getRouletteDiscountсCount(org.json.JSONObject userData)
    {
        jsonUtil = new JsonUtil();
        return Integer.parseInt(jsonUtil.getJSONKeyValueAsString(userData, "u:g:da:a:0:c"));
    }

    public List<Integer> getItemsListCurrentRouletteDiscount(org.json.JSONObject userData)
    {
        jsonUtil = new JsonUtil();
        JSONArray discountItems = jsonUtil.getJSONKeyValueAsJSONArray(userData, "dot:0:di:t");
        List<Integer> ListOfItemsCurrentRouletteDiscount = new ArrayList<Integer>();
        for (int i = 0; i < discountItems.length(); i++)
        {
            ListOfItemsCurrentRouletteDiscount.add(discountItems.getInt(i));
        }
        return ListOfItemsCurrentRouletteDiscount;
    }


    private int getRouletteDiscountType(org.json.JSONObject userData)
    {
        jsonUtil = new JsonUtil();
        return Integer.parseInt(jsonUtil.getJSONKeyValueAsString(userData, "dot:0:i"));
    }

    private double getRouletteDiscount(org.json.JSONObject userData)
    {
        jsonUtil = new JsonUtil();
        return Double.parseDouble(jsonUtil.getJSONKeyValueAsString(userData, "dot:0:c"));
    }

    private DiscountRouletteOfferKind getRouletteOfferKindTypeId(org.json.JSONObject userData)
    {
        jsonUtil = new JsonUtil();
        String TypeId = jsonUtil.getJSONKeyValueAsString(userData, "dot:0:k");
        for (DiscountRouletteOfferKind Id : DiscountRouletteOfferKind.values()) {
            if (Id.getKindId() == Integer.parseInt(TypeId))
            {
                return Id;
            }
        }
        return null;
    }

    //======================================DiscountOfferDataTroops==================================

    private TroopsKindId getUnitKindIdFromRouletteOffer(org.json.JSONObject userData)
    {
        jsonUtil = new JsonUtil();
        String KindId = jsonUtil.getJSONKeyValueAsString(userData, "dot:0:dt:k");
        if(KindId == null)
        {
            return null;
        }
        for (TroopsKindId Id : TroopsKindId.values()) {
            if (Id.getKindId() == Integer.parseInt(KindId))
            {
                return Id;
            }
        }
        return null;
    }

    private TroopsGroupId getGroupIdFromRouletteOffer(org.json.JSONObject userData)
    {
        jsonUtil = new JsonUtil();
        String GroupId = jsonUtil.getJSONKeyValueAsString(userData, "dot:0:dt:g");
        if(GroupId == null)
        {
            return null;
        }
        for (TroopsGroupId Id : TroopsGroupId.values()) {
            if (Id.getGroupId() == Integer.parseInt(GroupId))
            {
                return Id;
            }
        }
        return null;
    }

    private boolean geAllUnitParam(org.json.JSONObject userData)
    {
        jsonUtil = new JsonUtil();
        String  all = jsonUtil.getJSONKeyValueAsString(userData, "dot:0:dt:a");
        if (all != null)
        {
            if (Integer.parseInt(all) == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    private boolean geAllStrategiesUnitParam(org.json.JSONObject userData)
    {
        jsonUtil = new JsonUtil();
        String  allStrategies = jsonUtil.getJSONKeyValueAsString(userData, "dot:0:dt:s");
        if (allStrategies != null)
        {
            if (Integer.parseInt(allStrategies) == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public double expectedPriceCorrectionByRouletteDiscount(org.json.JSONObject userData,
                                                            Items item, //to do Object idOfObjectPurchases
                                                            int itemCount)
    {
        // to do DiscountRouletteOfferKind OfferKind = getRouletteOfferKindTypeId(userData);
        int discountType = getRouletteDiscountType(userData);
        double discount = getRouletteDiscount(userData);
        DiscountRouletteOfferKind OfferKind = getRouletteOfferKindTypeId(userData);

        double itemPrice = item.getPrice() * itemCount;
        Items[] discpuntItemsList = rouletteDiscountTypes.get(discountType);
        if (discpuntItemsList == null)
        {
            return itemPrice;
        }

        for( int i = 0; i<discpuntItemsList.length; i++)
        {
            if (discpuntItemsList[i].getId() == item.getId())
            {
                return itemPrice * discount;
            }
        }
        return itemPrice;
    }

    public double expectedPriceCorrectionForUnits(org.json.JSONObject userData,
                                                  org.json.JSONObject staticData,
                                                  int unitId) throws Exception
    {
        Sector sector = new Sector();

        HashMap AllUnits = sector.getAllUnitsFromStaticData(staticData);
        HashMap unitsPrices = sector.getUnitsPriceFromStaticData(staticData);
        TroopsGroupId discountPropertiesGroupId = getGroupIdFromRouletteOffer(userData);
        TroopsKindId discountPropertiesKindId = getUnitKindIdFromRouletteOffer(userData);
        double discount = getRouletteDiscount(userData);
        HashMap unitPriceInfo = (HashMap) unitsPrices.get(unitId);
        HashMap unitInfo = (HashMap) AllUnits.get(unitId);
        double unitPrice = (double) unitPriceInfo.get("g");
        int kindId = (int) unitInfo.get("k");
        int groupId = (int) unitInfo.get("g");

        //if all units
        if (geAllUnitParam(userData))
            {
                return unitPrice * discount;
            }
        else if (geAllStrategiesUnitParam(userData))
            {
                if (discountPropertiesGroupId.getGroupId() == groupId)
                {
                    return unitPrice * discount;
                }
                else
                {
                    return unitPrice;
                }
            }
        else
            {
                if (discountPropertiesKindId != null && discountPropertiesGroupId == null)
                    {
                        if (discountPropertiesKindId.getKindId() == kindId)
                            {
                                return unitPrice * discount;
                            }
                        else
                            {
                                return unitPrice;
                            }
                    }
                else if (discountPropertiesKindId == null && discountPropertiesGroupId != null)
                    {
                        if (discountPropertiesGroupId.getGroupId() == groupId)
                            {
                                return unitPrice * discount;
                            }
                        else
                            {
                                return unitPrice;
                            }
                    }
                else if (discountPropertiesKindId == null && discountPropertiesGroupId == null)
                    {
                        return unitPrice;
                    }
            else
                {
                    return unitPrice;
                }
            }
        }

}
