package utils;

import helpers.ServerMethods;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by guska on 16.04.16.
 */
public class HttpUtil {

    public String getPostRequestResponse (String url, String postBody, List<String[]> listOfheaders) throws IOException {

        CloseableHttpClient httpClient = HttpClients.createDefault();

        HttpPost httpPostRequest = new HttpPost(url);

        StringEntity entity = new StringEntity(postBody);

        httpPostRequest.setEntity(entity);

        for (int i = 0; i < listOfheaders.size(); i++)
        {
            httpPostRequest.setHeader(listOfheaders.get(i)[0], listOfheaders.get(i)[1]);
        }

        CloseableHttpResponse response = httpClient.execute(httpPostRequest);

        return getResponseBodyAsString(response);
    }

    public String getGetRequestResponse(String url, List<NameValuePair> listOfParameters) throws IOException, URISyntaxException {

        java.net.URI uri = new URIBuilder(url)
                .addParameters(listOfParameters)
                .build();

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(String.valueOf(uri));
        HttpResponse response = client.execute(request);

        return getResponseBodyAsString(response);
    }

    private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public String getResponseBodyAsString(HttpResponse response) throws IOException {

        String result = null;

        HttpEntity responseEntity = response.getEntity();

        if (responseEntity != null) {
            // A Simple JSON Response Read
            InputStream instream = responseEntity.getContent();
            result = convertStreamToString(instream);
            // now you have the string representation of the HTML request
            System.out.println("RESPONSE: " + response.toString());
            instream.close();
        }
        return result;
    }

    public String requestPatternForGBOCommand(String postBody, List<String[]> listOfheaders) throws IOException {

        String url = "https://gbo.x-plarium.com/games/console/ExecuteCommand";

        CloseableHttpClient httpClient = HttpClients.createDefault();

        HttpPost httpPostRequest = new HttpPost(url);

        StringEntity entity = new StringEntity(postBody);

        httpPostRequest.setEntity(entity);

        for (int i = 0; i < listOfheaders.size(); i++)
        {
            httpPostRequest.setHeader(listOfheaders.get(i)[0], listOfheaders.get(i)[1]);
        }

        CloseableHttpResponse response = httpClient.execute(httpPostRequest);

        return getResponseBodyAsString(response);
    }

}
