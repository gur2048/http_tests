package utils;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ResponseUtil {

    JsonUtil jsonUtil;

    public String responseJsonStringCut(String responsJson)
    {
        return responsJson.substring(1, responsJson.lastIndexOf("}")+1).trim();
    }

    public String getPostResponseAsString(CloseableHttpResponse httpPostResponse) throws IOException {
        String result = null;

        HttpEntity responseEntity = httpPostResponse.getEntity();

        if (responseEntity != null) {

            // A Simple JSON Response Read
            InputStream instream = responseEntity.getContent();
            result = convertStreamToString(instream);
            // now you have the string representation of the HTML request
            //System.out.println("RESPONSE: " + result);
            System.out.println("RESPONSE: " + httpPostResponse.toString());
            instream.close();
        }

        return result;
    }

    private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public Double getPriceFromServerResponce (org.json.JSONObject serverResponce)
    {
        jsonUtil = new JsonUtil();
        String priceFromServerResponce = jsonUtil.getJSONKeyValueAsString(serverResponce, "o:p:g");
        if (priceFromServerResponce != null)
        {
            return Double.parseDouble(priceFromServerResponce);
        }
        else
        {
            return null;
        }
    }

}
