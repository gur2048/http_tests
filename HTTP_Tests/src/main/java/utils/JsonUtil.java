package utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

public class JsonUtil {

    public JSONObject stringToJSON (String stringJson) throws JSONException
    {
        return new JSONObject(stringJson);
    }

    public static JSONArray getJSONKeyValueAsJSONArray(JSONObject jsonObject, String address)
    {
        String result = null;String[] jsonValueAddres = address.split(":");
        try {
            JSONArray resultArray = null;
            for (int i = 0; i < jsonValueAddres.length; i++) {
                if (resultArray == null)
                {
                    // if object is just string we change value in key
                    if (jsonObject.optJSONArray(jsonValueAddres[i]) == null
                            && jsonObject.optJSONObject(jsonValueAddres[i]) == null) {
                        return null;
                        // if it's jsonobject
                    } else if (jsonObject.optJSONObject(jsonValueAddres[i]) != null) {
                        jsonObject = ((JSONObject) jsonObject.get(jsonValueAddres[i]));
                        // if it's jsonarray
                    } else if (jsonObject.optJSONArray(jsonValueAddres[i]) != null) {
                        resultArray = jsonObject.getJSONArray(jsonValueAddres[i]);
                        if (i == jsonValueAddres.length-1)
                        {
                            return resultArray;
                        }
                    } else {
                        return null;
                    }
                }
                else
                {
                    // if it's jsonarray
                    jsonObject = (JSONObject) (resultArray.get(Integer.parseInt(jsonValueAddres[i])));
                    resultArray = null;
                }
            }
        }
        catch (JSONException e)
        {
            return null;
        }
        return null;
    }

    public String getStringJSONfromFile(String path) throws IOException {
        String requestBody = new String(Files.readAllBytes(Paths.get(path)));
        requestBody = requestBody.substring(1).trim();
        return requestBody;
    }

    public String getJSONKeyValueAsString(JSONObject jsonObject, String address) {
        String result = null;
        String[] jsonValueAddres = address.split(":");
        try {
            JSONArray resultArray = null;
            for (int i = 0; i < jsonValueAddres.length; i++) {
                if (resultArray == null)
                {
                        // if object is just string we change value in key
                    if (jsonObject.optJSONArray(jsonValueAddres[i]) == null
                            && jsonObject.optJSONObject(jsonValueAddres[i]) == null) {
                        result = jsonObject.get(jsonValueAddres[i]).toString();
                        // if it's jsonobject
                    } else if (jsonObject.optJSONObject(jsonValueAddres[i]) != null) {
                        jsonObject = ((JSONObject) jsonObject.get(jsonValueAddres[i]));
                        // if it's jsonarray
                    } else if (jsonObject.optJSONArray(jsonValueAddres[i]) != null) {
                        resultArray = jsonObject.getJSONArray(jsonValueAddres[i]);

                    } else {
                        return null;
                    }
                }
                else
                {
                    // if it's jsonarray
                    jsonObject = (JSONObject) (resultArray.get(Integer.parseInt(jsonValueAddres[i])));
                    resultArray = null;
                }
            }
        }
        catch (JSONException e)
        {
            return null;
        }
        return result;
    }

    public String changeJSON_byKeys(String StringJSON, List<String[]> ListKeys_NewValue_MarkerValue) throws Exception {

        JSONObject jsonObject = stringToJSON(StringJSON);

        for (int i = 0; i < ListKeys_NewValue_MarkerValue.size(); i++)
        {
            String[] masKeys_NewValue_MarkerValue = ListKeys_NewValue_MarkerValue.get(i);
            jsonObject = function(jsonObject, masKeys_NewValue_MarkerValue[0],
                                masKeys_NewValue_MarkerValue[1],
                                masKeys_NewValue_MarkerValue[2]);
        }
      return jsonObject.toString();
    }

    private static JSONObject function(JSONObject jsonObject, String keyMain, String newValue, String oldMain) throws Exception {
        // We need to know keys of Jsonobject
        JSONObject json = new JSONObject();
        Iterator iterator = jsonObject.keys();
        String key = null;
        while (iterator.hasNext()) {
            key = (String) iterator.next();
            // if object is just string we change value in key
            if ((jsonObject.optJSONArray(key) == null) && (jsonObject.optJSONObject(key) == null)) {
                if ((key.equals(keyMain)) && (jsonObject.get(key).toString().equals(oldMain))) {
                    // put new value
                    jsonObject.put(key, newValue);
                    return jsonObject;
                }
            }
            // if it's jsonobject
            if (jsonObject.optJSONObject(key) != null) {
                function(jsonObject.getJSONObject(key), keyMain, newValue, oldMain);
            }

            // if it's jsonarray
            if (jsonObject.optJSONArray(key) != null) {
                JSONArray jArray = jsonObject.getJSONArray(key);
                for (int i = 0; i < jsonObject.length(); i++) {
                    try
                    {
                        function(jArray.getJSONObject(i), keyMain, newValue, oldMain);
                    }catch (JSONException e)
                    {
                        //When JSONArray jArray = obj.getJSONArray(key) is not JSONObject
                    }
                }
            }
        }
        return jsonObject;
    }

    private String getJSONKeyValueAsString_old(JSONObject jsonObject, String address) {
        String result = null;
        String[] jsonValueAddres = address.split(":");
        try {
            for (int i = 0; i < jsonValueAddres.length; i++) {
                if (i + 1 >= jsonValueAddres.length) {
                    if (jsonValueAddres[i].matches("^\\D*$")) {
                        if (i != jsonValueAddres.length - 1) {
                            jsonObject = ((JSONObject) jsonObject.get(jsonValueAddres[i]));
                        } else {
                            result = jsonObject.get(jsonValueAddres[i]).toString();
                        }
                    } else {
                        JSONArray resultArray = getJSONKeyValueAsJSONArray(jsonObject, jsonValueAddres[i]);
                        jsonObject = (JSONObject) (resultArray.get(Integer.parseInt(jsonValueAddres[i + 1])));
                        i++;
                    }
                } else {
                    if (jsonValueAddres[i + 1].matches("^\\D*$")) {
                        if (i != jsonValueAddres.length - 1) {
                            jsonObject = ((JSONObject) jsonObject.get(jsonValueAddres[i]));
                        } else {
                            result = jsonObject.get(jsonValueAddres[i]).toString();
                        }
                    } else {
                        JSONArray resultArray = getJSONKeyValueAsJSONArray(jsonObject, jsonValueAddres[i]);
                        jsonObject = (JSONObject) (resultArray.get(Integer.parseInt(jsonValueAddres[i + 1])));
                        i++;
                    }
                }
            }
        }
        catch (ClassCastException e)
        {
            return "0";
        }
        return result;
    }


    public static JSONArray getJSONKeyValueAsJSONArray_old(JSONObject jsonObject, String address)
    {
        JSONArray result = null;
        String[] jsonValueAddres = address.split(":");
        for (int i = 0; i < jsonValueAddres.length; i++)
        {
            if (i != jsonValueAddres.length - 1)
            {
                jsonObject = ((JSONObject)jsonObject.get(jsonValueAddres[i]));
            }
            else
            {
                result = ((JSONArray)jsonObject.get(jsonValueAddres[i]));
            }
        }
        return result;
    }

}
