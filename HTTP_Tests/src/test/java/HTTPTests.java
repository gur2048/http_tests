import gbo.GboCommands;
import gbo.GboConnect;
import helpers.Items;
import helpers.ServerMethods;
import helpers.Servers;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import sector.Sector;
import utils.StaticDiscountUtil;

import java.util.Arrays;
import java.util.List;

public class HTTPTests extends AbstractTest{

	String signingSession;
	String requestBody;
	String responseJSON;
	String url;
	String gboCookie;
	org.json.JSONObject resultObject;
	Double discount;
	Double priceFromServerResponce;
	Double expectedPrice;
	int revision;
	Integer discountType;
	List<String[]> listOfKeys;
	org.json.JSONObject userData;
	org.json.JSONObject staticData;
	GboCommands gboCommands;
	GboConnect gboConnect;

	@BeforeTest
	public void beforeTest() throws Exception {

		gboCommands = new GboCommands();
		gboConnect = new GboConnect();
		sector = new Sector();

		gboCookie = gboConnect.gboLogin();
		url = Servers.ELVES_SUPER_TEST.getSuperTestServerUrl("01");

		staticData = sector.getStaticDataAsJSON(url);
		userData = sector.getUserDataAsJSON(url, "vk260523827");

		signingSession = jsonUtil.getJSONKeyValueAsString(userData, "k");
		revision = sector.getRevisionNum(userData);

		gboCommands.gboUserSetLevel(
				40,
				"vk260523827",
				Servers.ELVES_SUPER_TEST.getConsoleServerId(),
				gboCookie);
		gboCommands.gboUserResetAllPurschasesCount(
				"vk260523827",
				Servers.ELVES_SUPER_TEST.getConsoleServerId(),
				gboCookie);
		gboCommands.gboUserGiveGoldMoney(
				0,
				"vk260523827",
				Servers.ELVES_SUPER_TEST.getConsoleServerId(),
				gboCookie );
	}

	@DataProvider(name = "buyUnitsTest")
	public static Object[][] buyUnitsTest() {
		return new Object[][]{
				{13, 3 , Servers.ELVES_SUPER_TEST.getSuperTestServerUrl("01")},
				{14, 3 , Servers.ELVES_SUPER_TEST.getSuperTestServerUrl("01")},
				{15, 3 , Servers.ELVES_SUPER_TEST.getSuperTestServerUrl("01")},
				{16, 3 , Servers.ELVES_SUPER_TEST.getSuperTestServerUrl("01")},
				{16, 5 , Servers.ELVES_SUPER_TEST.getSuperTestServerUrl("01")}
		};
	}

	//@Test
	public void BlackMarketBuyItem701Test() throws Exception {

		//============================Test options=======================
		Items item = Items.MOVEMENT_BOST_050;
		int count = 10;

		String url = Servers.ELVES_SUPER_TEST.getSuperTestServerUrl("01");

		List<String[]> listOfheaders = Arrays.asList(
				new String[]{"server-method", ServerMethods.BLACK_MARKET_BUY_ITEM.getMethodName()},
				new String[]{"Content-type", "text/html"},
				new String[]{"signin-userId", "vk260523827"},
				new String[]{"signin-authKey", "ba27b979dc7980f41e0d77fe65b59068"},
				new String[]{"Access-key", "B5CC2AC393234bf89DDB5AA216B64668"},
				new String[]{"sign-code", "3a9ef6259272ff2b878bc6e4d93e6c08"},
				new String[]{"signin-session", signingSession}
				);

		listOfKeys = Arrays.asList(
				new String[]{"c", Integer.toString(count)       , "000"},
				new String[]{"i", Integer.toString(item.getId()), "000"},
				new String[]{"r", Integer.toString(revision)    , "000"}
		);

		requestBody = jsonUtil.changeJSON_byKeys(
				jsonUtil.getStringJSONfromFile("src/test/resources/MethodsBody/BlackMarketBuyItem.txt"),
				listOfKeys);

		//==============Send Request and get responce body as String==================
		responseJSON = httpUtil.getPostRequestResponse(
				url,
				requestBody,
				listOfheaders);

		//============================Get Result=========================
		resultObject = jsonUtil.stringToJSON(responseUtil.responseJsonStringCut(responseJSON));

		priceFromServerResponce = responseUtil.getPriceFromServerResponce(resultObject);
		expectedPrice = discountUtil.expectedPriceCorrectionByRouletteDiscount(
				userData,
				item,
				count);

		//============================Assert=============================
		softAssert.assertEquals(priceFromServerResponce, expectedPrice);
		softAssert.assertAll();
	}

	//@Test
	public void BlackMarketBuyItem700Test() throws Exception {

		//============================Test options=======================
		Items item = Items.MOVEMENT_BOST_075;
		int count = 10;
		String url = Servers.ELVES_SUPER_TEST.getSuperTestServerUrl("01");

		List<String[]> listOfheaders = Arrays.asList(
				new String[]{"server-method", ServerMethods.BLACK_MARKET_BUY_ITEM.getMethodName()},
				new String[]{"Content-type", "text/html"},
				new String[]{"signin-userId", "vk260523827"},
				new String[]{"signin-authKey", "ba27b979dc7980f41e0d77fe65b59068"},
				new String[]{"Access-key", "B5CC2AC393234bf89DDB5AA216B64668"},
				new String[]{"sign-code", "3a9ef6259272ff2b878bc6e4d93e6c08"},
				new String[]{"signin-session", signingSession}
		);

		listOfKeys = Arrays.asList(
				new String[]{"c", Integer.toString(count)       , "000"},
				new String[]{"i", Integer.toString(item.getId()), "000"},
				new String[]{"r", Integer.toString(revision)    , "000"}
		);

		requestBody = jsonUtil.changeJSON_byKeys(
				jsonUtil.getStringJSONfromFile("src/test/resources/MethodsBody/BlackMarketBuyItem.txt"),
				listOfKeys);

		requestBody = jsonUtil.changeJSON_byKeys(
				jsonUtil.getStringJSONfromFile("src/test/resources/MethodsBody/BlackMarketBuyItem.txt"),
				listOfKeys);

		//==============Send Request and get responce body as String==================
		responseJSON = httpUtil.getPostRequestResponse(
				url,
				requestBody,
				listOfheaders);

		//============================Get Result=========================
		resultObject = jsonUtil.stringToJSON(responseUtil.responseJsonStringCut(responseJSON));

		priceFromServerResponce = responseUtil.getPriceFromServerResponce(resultObject);
		expectedPrice = discountUtil.expectedPriceCorrectionByRouletteDiscount(
				userData,
				item,
				count);

		//============================Assert=============================
		softAssert.assertEquals(priceFromServerResponce, expectedPrice);
		softAssert.assertAll();
	}

	@Test(dataProvider = "buyUnitsTest", priority = 0)
	public void buyUnitsTest(int unitId, int unitsCount, String serverUrl) throws Exception {

		//============================Test options=======================
		List<String[]> listOfheaders = Arrays.asList(
				new String[]{"server-method", ServerMethods.BLACK_MARKET_BUY.getMethodName()},
				new String[]{"Content-type", "text/html"},
				new String[]{"signin-userId", "vk260523827"},
				new String[]{"signin-authKey", "ba27b979dc7980f41e0d77fe65b59068"},
				new String[]{"Access-key", "B5CC2AC393234bf89DDB5AA216B64668"},
				new String[]{"sign-code", "3a9ef6259272ff2b878bc6e4d93e6c08"},
				new String[]{"signin-session", signingSession}
		);

		listOfKeys = Arrays.asList(
				new String[]{"i", Integer.toString(unitId), "000"},
				new String[]{"n", Integer.toString(unitsCount) , "000"},
				new String[]{"r", Integer.toString(revision)    , "000"}
		);

		requestBody = jsonUtil.changeJSON_byKeys(
				jsonUtil.getStringJSONfromFile("src/test/resources/MethodsBody/BlackMarketBuyUnits.txt"),
				listOfKeys);

		//==============Send Request and get responce body as String==================
		responseJSON = httpUtil.getPostRequestResponse(
				serverUrl,
				requestBody,
				listOfheaders);

		//============================Get Result=========================
		resultObject = jsonUtil.stringToJSON(responseUtil.responseJsonStringCut(responseJSON));

		priceFromServerResponce = responseUtil.getPriceFromServerResponce(resultObject);
		expectedPrice = discountUtil.expectedPriceCorrectionForUnits(
				userData,
				staticData,
				unitId);

		//============================Assert=============================
		double a = Math.round(expectedPrice * unitsCount);// убрать эту дичь
		softAssert.assertEquals(priceFromServerResponce, a);
		softAssert.assertAll();
	}

	@AfterMethod
	public void revisionNumCorrection()
	{
		revision++;
	}

}