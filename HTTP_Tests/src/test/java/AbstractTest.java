import sector.Sector;
import utils.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeSuite;
import org.testng.asserts.SoftAssert;

public class AbstractTest {

	public static final int DEFAULT_TIME_OUT_IN_SECONDS = 20;

	protected WebDriverContainer container;

	protected static WebDriver driver;
	protected SoftAssert softAssert;
	protected HttpUtil httpUtil;
	protected JsonUtil jsonUtil;
	protected ResponseUtil responseUtil;
	protected RouletteDiscountUtil discountUtil;
	protected Sector sector;

	@BeforeSuite
	public void beforeSuite() {
		//container = new WebDriverContainer();
		//driver = container.getDriver();
		//driver.manage().timeouts().implicitlyWait(DEFAULT_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
		//driver.manage().window().maximize();
		softAssert = new SoftAssert();
		responseUtil = new ResponseUtil();
		discountUtil = new RouletteDiscountUtil();
		httpUtil = new HttpUtil();
		jsonUtil = new JsonUtil();
	}

	//@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
		responseUtil = new ResponseUtil();
		discountUtil = new RouletteDiscountUtil();
		httpUtil = new HttpUtil();
		jsonUtil = new JsonUtil();
	}

	//@AfterSuite
	public void afterSuite() {
		driver.quit();
	}



}
